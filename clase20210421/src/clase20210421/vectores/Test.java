package clase20210421.vectores;

import java.util.Random;

public class Test {

	public static void main(String[] args) {
		// Vectores estructuras de almacenamiento fijas
//		int[] vectorX = new int[6];
//		
//
//		for (int i : vectorX) {
//			System.out.println(i);
//		}
//		vectorX[0] = 10;
//		System.out.println(vectorX[0]);
//		
//		System.out.println(vectorX.length);

		
		
//		Camion c1 = new Camion(1000);
//		c1.setCargaActual(200);
//		
//		Pasajero a = new Pasajero(72);
//		
//		c1.subirPasajero(a);
//		
//		float consumoCamion = c1.recorrer(200);
//		System.out.println(consumoCamion);
		
		Random rnd = new Random();
		
		Camion c1 = new Camion(1000);
		c1.setCargaActual(rnd.nextFloat() * 1000);
		
		Pasajero a = new Pasajero(rnd.nextFloat() * 150 + 50);		// El peso del pasajero este entre 50 y 200
		c1.subirPasajero(a);
		
		float kilometros = rnd.nextFloat() * 1000 + 100;
		float consumo = c1.recorrer(kilometros);
		System.out.println(kilometros);
		System.out.println(consumo);
		
	}

}
