package clase20210421.vectores;

public class Camion extends Vehiculo {
	private float cargaMaxima, cargaActual;
	
	public Camion(float cargaMaxima) {
		super(1);
		
		this.cargaMaxima = cargaMaxima;
	}
	
	public float getCargaActual() {
		return cargaActual;
	}

	public void setCargaActual(float carga) {
		if (carga >=0 && carga <= this.cargaMaxima )
			this.cargaActual = carga;
		else
			System.err.println("Carga no permitida");
	}

	public float getCargaMaxima() {
		return cargaMaxima;
	}

	public float recorrer(float kilometros) {
		float consumo = 0.5f; // Arranque
		consumo += (kilometros * this.cargaActual * 0.01);
		consumo += (kilometros * this.pesoPasajeros() * 0.01);
		return consumo;
	}
}
