package clase20210421.vectores;

public abstract class Vehiculo {
	private String identificacion, marca, modelo;
	protected Pasajero[] pasajeros;
	
	public Vehiculo(int cantidadPasajeros) {
		this.pasajeros = new Pasajero[cantidadPasajeros];
	}
	
	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	public abstract float recorrer(float kilometros);
	
	public float pesoPasajeros() {
		float pesoTotal = 0;
		
		for (Pasajero p : this.pasajeros) {
			if (p != null)
				pesoTotal += p.getPeso();
		}
		
		return pesoTotal;
	}
	
	public boolean subirPasajero(Pasajero pasajero) {
		int indice = 0;
		while (indice < this.pasajeros.length && this.pasajeros[indice] != null)
			indice++;
		
		if (indice < this.pasajeros.length) {
			this.pasajeros[indice] = pasajero;
			return true;
		}
		
		return false;	
	}
}
