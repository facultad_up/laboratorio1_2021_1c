package clase20210421.vectores;

public class Colectivo extends Vehiculo {
	
	public Colectivo() {
		super(25);
	}

	public float recorrer(float kilometros) {
		float consumo = 0.25f; // Arranque
		consumo += (kilometros * this.pesoPasajeros() * 0.005);
		return consumo;
	}
}
