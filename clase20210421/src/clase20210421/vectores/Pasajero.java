package clase20210421.vectores;

public class Pasajero {
	private float peso;

	public Pasajero(float peso) {
		this.peso = peso;
	}

	public float getPeso() {
		return peso;
	}
	
}
