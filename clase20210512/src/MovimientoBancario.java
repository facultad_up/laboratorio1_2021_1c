
public class MovimientoBancario {
	private String detalle;
	private float importe;
	private String fecha;
	
	public MovimientoBancario(String detalle, float importe, String fecha) {
		this.detalle = detalle;
		this.importe = importe;
		this.fecha = fecha;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public float getImporte() {
		return importe;
	}

	public void setImporte(float importe) {
		this.importe = importe;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String toString() {
		return "MovimientoBancario [detalle=" + detalle + ", importe=" + importe + ", fecha=" + fecha + "]";
	}
	
	
	
}
