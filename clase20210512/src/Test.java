import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class Test {

	public static void main(String[] args) {
		Cuenta c = new Cuenta("Nacion", 222222);
		Cuenta a = new Cuenta("Nacion", 222232);
		
		System.out.println(c.equals(a));
		
		
		ArrayList<Cuenta> cuentas = new ArrayList<Cuenta>();
		
		Cuenta c1 = new Cuenta("uno", 9563, 100.1f);
		Cuenta c2 = new Cuenta("dos", 89456, 200.2f);
		Cuenta c3 = new Cuenta("tres", 1659, 300.3f);
		Cuenta c4 = new Cuenta("uno", 8749, 400.4f);
		Cuenta c5 = new Cuenta("dos", 5484, 500.5f);
		
		cuentas.add(c1);
		cuentas.add(c2);
		cuentas.add(c3);
		cuentas.add(c4);
		cuentas.add(c5);
		cuentas.add(c3);
		cuentas.add(c3);
		cuentas.add(c3);
		
		for (Cuenta cuenta : cuentas) {
			System.out.println(cuenta);
		}
		
		System.out.println("------------------------------------");
		
		// Clase anonima
		Comparator<Cuenta> comp = new Comparator<Cuenta>() {

			public int compare(Cuenta c1, Cuenta c2) {
				if (c1.getNombreBanco().equals(c2.getNombreBanco())) {
					return new Float(c1.getSaldo()).compareTo(new Float(c2.getSaldo()));
				}
				
				return c1.getNombreBanco().compareTo(c2.getNombreBanco());
			}
		}; 
		
		Collections.sort(cuentas, comp);
		
		System.out.println("Ordenado por NOMBREBANCO-SALDO");
		
		for (Cuenta cuenta : cuentas) {
			System.out.println(cuenta);
		}
		
		System.out.println("------------------------------------");
		
		Collections.sort(cuentas, new Comparator<Cuenta>() {

			public int compare(Cuenta c1, Cuenta c2) {
				if (c1.getNombreBanco().equals(c2.getNombreBanco())) {
					return c1.getNumeroCuenta() - c2.getNumeroCuenta();
				}
				
				return c1.getNombreBanco().compareTo(c2.getNombreBanco());
			}
		});
		
		System.out.println("Ordenado por NOMBREBANCO-NUMEROCUENTA");
		
		for (Cuenta cuenta : cuentas) {
			System.out.println(cuenta);
		}
		
	}

}
