import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Conjuntos {

	public static void main(String[] args) {
		Cuenta c1 = new Cuenta("uno", 9563, 100.1f);
		Cuenta c2 = new Cuenta("dos", 89456, 200.2f);
		Cuenta c3 = new Cuenta("tres", 1659, 300.3f);
		Cuenta c4 = new Cuenta("uno", 8749, 400.4f);
		Cuenta c5 = new Cuenta("dos", 5484, 500.5f);
		
		Cuenta c30 = new Cuenta("tres", 1659, 84900.3f);
		
		HashSet<Cuenta> cuentas = new HashSet<Cuenta>();
		
		cuentas.add(c1);
		cuentas.add(c2);
		cuentas.add(c3);
		cuentas.add(c4);
		cuentas.add(c5);
		
		cuentas.add(c30);
		
		System.out.println(c3.equals(c30));
		
		for (Cuenta cuenta : cuentas) {
			System.out.println(cuenta);
		}
		
		cuentas.remove(c30);
		
		System.out.println("----------------------");
		
		for (Cuenta cuenta : cuentas) {
			System.out.println(cuenta);
		}
		
		System.out.println("----------------------");
		
		// Map o Diccionarios: Clave-Valor
		
		HashMap<String, Cuenta> diccionario = new HashMap<String, Cuenta>();
		diccionario.put("c1", c1);
		diccionario.put("c2", c2);
		diccionario.put("c3", c3);
		diccionario.put("c4", c4);
		diccionario.put("c5", c5);
		
		for (String clave : diccionario.keySet()) {
			System.out.println(clave + "\t" + diccionario.get(clave));
		}
		
		System.out.println(diccionario.get("c2222"));
		
		
		System.out.println("----------------------");

		HashMap<Cuenta, ArrayList<MovimientoBancario>> movimientos = new HashMap<Cuenta, ArrayList<MovimientoBancario>>();
		
		movimientos.put(c1, new ArrayList<MovimientoBancario>());
		movimientos.get(c1).add(new MovimientoBancario("Apertura", 500, "20210512"));
		movimientos.get(c1).add(new MovimientoBancario("Compra Speed", -70, "20210512"));
		movimientos.get(c1).add(new MovimientoBancario("Impuesto", -5, "20210512"));
		
		for (MovimientoBancario mv : movimientos.get(c1)) {
			System.out.println(mv);
		}
		
		
	}

}
