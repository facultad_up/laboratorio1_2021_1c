

public class Cuenta implements Comparable<Cuenta> {
	private String nombreBanco;
	private int numeroCuenta;
	private float saldo;
	
	public Cuenta(String nombreBanco, int numeroCuenta) {
		this.nombreBanco = nombreBanco;
		this.numeroCuenta = numeroCuenta;
	}
	
	public Cuenta(String nombreBanco, int numeroCuenta, float saldo) {
		this.nombreBanco = nombreBanco;
		this.numeroCuenta = numeroCuenta;
		this.saldo = saldo;
	}

	public String getNombreBanco() {
		return nombreBanco;
	}

	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}

	public int getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(int numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public float getSaldo() {
		return saldo;
	}

	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}

	public String toString() {
		return "Cuenta: nombreBanco=" + nombreBanco + ", numeroCuenta=" + numeroCuenta + ", saldo=" + saldo;
	}
	
	public boolean equals(Object o) {
		return o != null && o instanceof Cuenta && ((Cuenta)o).nombreBanco.equals(this.nombreBanco) && ((Cuenta)o).numeroCuenta == this.numeroCuenta;
	}
	
	public int hashCode() {
		return this.nombreBanco.hashCode() + this.numeroCuenta * 2;
	}
	
//	public boolean equals(Object o) {
//		if (o == null)
//			return false;
//		
//		if (!(o instanceof Cuenta))
//			return false;
//		
//		return ((Cuenta)o).nombreBanco.equals(this.nombreBanco) && ((Cuenta)o).numeroCuenta == this.numeroCuenta;;
//	}

	public int compareTo(Cuenta cuenta) {
		/*
		 * menor a 0	: Si considero el objeto menor al recibido
		 * 0 			: Se consideran iguales
		 * mayor a 0 	: Si considero el objeto mayor al recibido
		 */
		
		return this.nombreBanco.compareTo(cuenta.nombreBanco);
	}
}
