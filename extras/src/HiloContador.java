
public class HiloContador extends Thread {
	private String nombre;
	private int desde, hasta, espera;
	

	public HiloContador(String nombre, int desde, int hasta, int espera) {
		this.nombre = nombre;
		this.desde = desde;
		this.hasta = hasta;
		this.espera = espera;
	}
	
	public void run() {
		for(int i = desde; i < hasta; i++) {
			
			System.out.println(nombre + "\t" + i);
			
			try {
				Thread.sleep(espera);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		HiloContador hcA = new HiloContador("A", 0 , 10, 1000);
		hcA.start();
		
		HiloContador hcB = new HiloContador("B", 0 , 10, 50);
		hcB.start();
		
		System.out.println("Antes");
		hcB.join(); //Espera la finalizacion de B
				    //para seguir
		System.out.println("Despues");
		
		
	}
	
}
