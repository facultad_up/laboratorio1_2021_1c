import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BaseDatos1 {

    public static void main(String[] args) {
        
        Connection conn = null;
        
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:basedatos.db");
            
            System.out.println("Me conecte!");
            
            File f = new File("basedatos.db");
            if (f.length() == 0) {
                
                System.out.println("Base vacia, creando base...");
                
                String sql = "CREATE TABLE Alumno (Nombre Text, Apellido Text, Legajo int)";
                
                Statement smt = conn.createStatement();
                smt.execute(sql);
                
                System.out.println("Tabla creada :)");
            }
            
            Statement smt = conn.createStatement();
            smt.execute("INSERT INTO ALUMNO (Nombre, Apellido, Legajo) VALUES ('Juan', 'perez', 3333) ");
            
            
            conn.createStatement().execute("INSERT INTO ALUMNO (Nombre, Apellido, Legajo) VALUES ('xxxx', 'perez2', 3434) ");
            
            
            
            PreparedStatement prep = conn.prepareStatement("INSERT INTO ALUMNO (Nombre, Apellido, Legajo) VALUES (?, ?, ?) ");
            prep.setString(1, "Jimmy");
            prep.setString(2, "Dos");
            prep.setString(3, "333333");
            prep.execute();
            
            
            smt = conn.createStatement();
            ResultSet rs = smt.executeQuery("SELECT * FROM Alumno");
            while (rs.next()) {
                System.out.print(rs.getString(1));
                System.out.println("\t" + rs.getString("Apellido"));
            }
            rs.close();
            
            
            
        } catch(Exception e) {
            
            System.out.println(e);
            
        } finally {
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }

    }

}
