package clase20210331.ejemplos1;

public class Contador {
	private static int numero;
	private String nombre;
	
	public Contador(String nombre) {
		this.nombre = nombre;
	}

	public int getNumero() {
		return numero;
	}

	private void incrementar() {
		Contador.numero++;
	}
	
	private static void mostrarCuenta() {
		System.out.println("La cuenta va por: " + numero);
	}
	
	public String toString() {
		return this.nombre + " = " + Contador.numero;
	}
	
	
	public static void main(String[] args) {
		
		Contador a = new Contador("Uno");
		a.incrementar();
		a.incrementar();
		
		Contador b = new Contador("Dos");
		b.incrementar();
		b.incrementar();
		
		System.out.println(a);
		System.out.println(b);

		System.out.println(Contador.numero);

		Contador.mostrarCuenta();
	}
}
