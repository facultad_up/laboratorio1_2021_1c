package clase20210331.ejemplos1;

public class Persona {

	private String nombre, apellido;
	private int edad;
	private String telefono;

	public Persona(String nombre, String apellido, int edad, String telefono) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
		this.telefono = telefono;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String toString() {
		return "Persona [nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad + ", telefono=" + telefono
				+ "]";
	}

	public boolean equals(Object o) {
//		if (o instanceof Persona) {
//			Persona persona = (Persona) o;
//			if (this.nombre.equals(persona.nombre) && this.apellido.equals(persona.apellido)) {
//				return true;
//			}
//		}
//		
//		return false;

		return (o instanceof Persona) && this.nombre.equals(((Persona)o).nombre) && this.apellido.equals(((Persona)o).apellido);
	}

	public static void main(String[] args) {
		Persona a = new Persona("Uno", "One", 1, "11-1-111111");
		Persona b = new Persona("Uno", "One", 1, "11-1-111111");

		String x = "Uno";

		if (a.equals(x)) {
			System.out.println("Son iguales");
		} else {
			System.out.println("Son distintos");
		}

	}
}
