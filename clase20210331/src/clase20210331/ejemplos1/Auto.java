package clase20210331.ejemplos1;

public final class Auto extends Vehiculo {
	
	public Auto(String modelo, String marca, String patente) {
		super(modelo, marca, patente);
	}

// No se puede sobreescribir por ser final
//	public void arrancar() {
//		System.out.println("No arranco");
//	}
	
	public static void main(String[] args) {
		Auto a = new Auto("3008", "Pegeout", "AA555BB");
		a.arrancar();
	}
}

// No se puede heredar una clase definida como final
//class AutoDeportivo extends Auto {
//
//	public AutoDeportivo(String modelo, String marca, String patente) {
//		super(modelo, marca, patente);
//	}
//	
//}