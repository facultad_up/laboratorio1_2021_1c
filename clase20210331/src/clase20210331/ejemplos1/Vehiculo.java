package clase20210331.ejemplos1;

public abstract class Vehiculo {
	private final String modelo, marca;
	private String patente;
	private boolean enMarcha;
	
	public Vehiculo(String modelo, String marca, String patente) {
		this.modelo = modelo;
		this.marca = marca;
		this.patente = patente;
	}
	
	public String getModelo() {
		return modelo;
	}

	public String getMarca() {
		return marca;
	}

	public String getPatente() {
		return patente;
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}

	public boolean isEnMarcha() {
		return enMarcha;
	}

	public final void arrancar() {
		System.out.println("Poniendo en marcha...");
		this.enMarcha = true;
	}
	
}
