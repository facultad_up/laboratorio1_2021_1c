package clase20210331.ejemplos1;

public class TestContadorUnico {

	public static void main(String[] args) {

//		ContadorUnico a = new ContadorUnico();
//		ContadorUnico b = new ContadorUnico();
//		ContadorUnico c = new ContadorUnico();
		
		ContadorUnico.getInstance().incrementar();
		ContadorUnico.getInstance().incrementar();
		System.out.println(ContadorUnico.getInstance().getContador());
	}
}
