package clase20210331.ejemplos1;

/*
 * Patron de dise�o: 
 * - Problematica con determinadas caracteristicas.
 * - Propone una solucion determinada.
 */

/* Patron: Singleton
 * 1- Poner en privado el Constructor.
 * 2- Crear un metodo estatico para acceder a la unica instancia
 * 3- Crear la variable de la instancia estatica
 * 4- Instanciar la unica instancia de la clase.
 */

public class ContadorUnico {
	private static ContadorUnico instance;
	
	private int contador;
	
	
	private ContadorUnico() {
		
	}
	
	public static ContadorUnico getInstance() {
		if (instance == null)
			instance = new ContadorUnico();
		
		return instance;
	}
	
	public void incrementar() {
		this.contador++;
	}
	
	public int getContador() {
		return this.contador;
	}
}
