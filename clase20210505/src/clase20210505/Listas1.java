package clase20210505;

import java.util.ArrayList;

class Test {
	private int numero;

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
}

public class Listas1 {

	public static void main(String[] args) {
		int[] x = new int[10];
		
		x[2] = 1;		// x es puntero a memoria, se almacenan 10 numeros enteros
						// Aritmetica de punteros: x + 2 * sizeof(int)

		
		ArrayList al = new ArrayList();
		
		al.add("Elemento 1");
		al.add(123);
		
		Test t = new Test();
		
		al.add(t);
		
		System.out.println(al);
		
		ArrayList<Number> numeros = new ArrayList<Number>();
		numeros.add(123);
		// numeros.add("aa"); Ya no se puede, pq espera un Number
		
		
		
	}

}
