package clase20210505;

import java.util.Comparator;

public class ComparadorCuentaPorSaldo implements Comparator<Cuenta> {

	public int compare(Cuenta cuenta1, Cuenta cuenta2) {
		/*
		 * menor a 0	: Si considero el objeto "cuenta1" menor a "cuenta2"
		 * 0 			: Se consideran iguales
		 * mayor a 0 	: Si considero el objeto "cuenta1" mayor a "cuenta2"
		 */
		
		if (cuenta1.getSaldo() == cuenta2.getSaldo())
			return 0;
		
		if (cuenta1.getSaldo() < cuenta2.getSaldo())
			return -1;
		
		return 1;
		
//		return new Float(cuenta1.getSaldo()).compareTo(new Float(cuenta2.getSaldo()));
		
	}
	
}
