package clase20210505;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Lista3 {
	public static void main(String[] args) {
		ArrayList<Cuenta> cuentas = new ArrayList<Cuenta>();
		
		Cuenta c1 = new Cuenta("uno", 1111, 100.1f);
		Cuenta c2 = new Cuenta("dos", 2222, 200.2f);
		Cuenta c3 = new Cuenta("tres", 3333, 300.3f);
		Cuenta c4 = new Cuenta("cuatro", 4444, 400.4f);
		Cuenta c5 = new Cuenta("cinco", 5555, 500.5f);
		
		cuentas.add(c1);
		cuentas.add(c2);
		cuentas.add(c3);
		cuentas.add(c4);
		cuentas.add(c5);
		
		for (Cuenta cuenta : cuentas) {
			System.out.println(cuenta);
		}
		
		System.out.println("-----------------");
		Scanner teclado = new Scanner(System.in);
		
		System.out.print("Banco: ");
		String banco = teclado.next();
		
		System.out.print("Cuenta: ");
		int numeroCuenta = teclado.nextInt();
		
		Cuenta cuentaAEliminar = new Cuenta(banco, numeroCuenta);
		
		cuentas.remove(cuentaAEliminar);
		
		for (Cuenta cuenta : cuentas) {
			System.out.println(cuenta);
		}
		
		System.out.println("-----------------");
		
		Collections.sort(cuentas);
		
		for (Cuenta cuenta : cuentas) {
			System.out.println(cuenta);
		}
		
		System.out.println("-----------------");
		
		Collections.sort(cuentas, new ComparadorCuentaPorSaldo());
		
		for (Cuenta cuenta : cuentas) {
			System.out.println(cuenta);
		}
		
	}
}
