package clase20210505.generico;

public class Auto<T extends Motor> {
	private T motor;
	
	public Auto() {
		
	}
	
	public void ensamblarMotor(T motor) {
		this.motor = motor;
	}
	
	public void andar() {
		//this.motor.funcionar();
	}
}
