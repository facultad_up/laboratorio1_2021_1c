package clase20210505.generico;

public class Test {

	public static void main(String[] args) {
//		Auto a = new Auto(new Motor());
//		a.andar();
		
//		Auto<String> x = new Auto<String>("Hola");
//		Auto<Boolean> y = new Auto<Boolean>(true);
		
		Auto<MotorV8> a = new Auto<MotorV8>();
		MotorV8 m = new MotorV8();
		a.ensamblarMotor(m);
		
	}

}
