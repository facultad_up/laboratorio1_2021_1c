package clase20210505;

import java.util.ArrayList;
import java.util.Scanner;

public class Lista2 {

	public static void main(String[] args) {
		/*
		 * Leer desde teclado las cuentas, hasta que se ingrese un texto "fin" en nombre de banco.
		 */
		
		ArrayList<Cuenta> cuentas = new ArrayList<Cuenta>();
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.print("Ingrese nombre banco: ");
		String nombreBanco = teclado.next();
		while (!nombreBanco.equals("fin")) {
			
			System.out.print("Ingrese numero de cuenta: ");
			int numeroCuenta = teclado.nextInt();
			
			System.out.print("Ingrese saldo: ");
			float saldo = teclado.nextFloat();		
			
			Cuenta cuenta = new Cuenta(nombreBanco, numeroCuenta);
			cuenta.setSaldo(saldo);
			
			cuentas.add(cuenta);
			
			
			System.out.print("Ingrese nombre banco: ");
			nombreBanco = teclado.next();
		}
		
		
		System.out.println("---------------------------------");
		System.out.println("Listado de cuentas");
		for (Cuenta cuenta : cuentas) {
			System.out.println(cuenta.getNombreBanco() + "\t" + cuenta.getNumeroCuenta() + "\t" + cuenta.getSaldo());
		}
		

	}

}
