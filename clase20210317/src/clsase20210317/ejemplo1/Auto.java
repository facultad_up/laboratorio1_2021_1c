package clsase20210317.ejemplo1;

public class Auto extends Vehiculo {
	private int velocidadMaxima;
	
	public Auto(int velocidadMaxima) {
		super();  
		this.velocidadMaxima = velocidadMaxima;
		System.out.println("Construyendo auto.");
	}
	
	// Implementando el metodo abstract heredado
	public void cargar(float carga) {
		this.cargaActual += carga;
	}
	
	public void abrirTecho() {
		System.out.println("Abriendo techo...");
	}
	
	public int nivelSapito() {
		// Proceso que calcula el nivel del sapito
		return 27;
	}
}
