package clsase20210317.ejemplo1;

public abstract class Vehiculo {
	protected String patente;
	protected float cargaMaxima, cargaActual;
	protected int modelo;
	
	public Vehiculo() {
		System.out.println("Construyendo vehiculo");
	}
	
	public abstract void cargar(float carga);
	
	public void andar() {
		System.out.println("Andando...");
	}
}
