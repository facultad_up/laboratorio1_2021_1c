package clsase20210317.ejemplo1;

public class Test {

	public static void main(String[] args) {
		// Vehiculo a = new Vehiculo();
		
		Auto a = new Auto(120);
		a.cargar(20);
		a.abrirTecho();
		
		Vehiculo b = new Auto(110);
		b.cargar(12);
		// b.abrirTecho(); No se puede
		
		// Cast - Amoldar - DOWNCasting
		((Auto)b).abrirTecho();
		
		((Auto)b).nivelSapito();
		
		
		
		
		Peaje p = new Peaje();
		float costoPeaje = p.calcularValorPeaje(b);
		System.out.println(costoPeaje);
		
		costoPeaje = p.calcularValorPeaje(a);
		System.out.println(costoPeaje);
		
		
		System.out.println("---------------------");
		AutoDeportivo ad = new AutoDeportivo(5000000);
		
		Vehiculo v1 = new AutoDeportivo(1500000);
		
		
	}

}
