package manejoerrores;

import java.io.FileNotFoundException;
import java.io.IOException;

class MiException extends Exception {

	public MiException(String quepaso) {
		super(quepaso);
	}
}

public class ManejoExc1 {

	public static void otroMetodo() throws MiException, FileNotFoundException {

		MiException miException = new MiException("Aca pas� algo feo");

		throw miException;
	}

	public static void main(String[] args) {
		System.out.println("Comienzo programa.");

		try {
			System.out.println("En el try...");
//			int a = 0;
//			int c = 5 / a;
			otroMetodo();
			System.out.println("... fin try");
		} catch (MiException m) {
			
		} catch (FileNotFoundException fnt) {
			
		} catch (IOException ioe) {
			
		} catch (Exception e) {
		
			System.out.println("En el catch! " + e.getMessage());
		}

		System.out.println("Fin programa.");
	}

}
