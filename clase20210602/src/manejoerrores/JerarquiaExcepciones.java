package manejoerrores;

import java.util.Random;

class CodigoClienteException extends Exception {

	public CodigoClienteException(String string) {
		super(string);
	}

}

class DireccionException extends Exception {

	public DireccionException(String string) {
		super(string);
	}

}

public class JerarquiaExcepciones {

	public static void metodo1() throws CodigoClienteException, DireccionException {
		Random rnd = new Random();

//		int a = 0;
//		int b = 3/a;
		
		if (rnd.nextBoolean())
			throw new CodigoClienteException("Codigo cliente invalido");

		throw new DireccionException("Direccion invalida");
	}

	public static void main(String[] args) {

		try {
			metodo1();
		} catch (CodigoClienteException e) {
			System.err.println("Falla Codigo: " + e.getMessage());
		} catch (DireccionException e) {
			System.err.println("Falla direccion: " + e.getMessage());
		} catch(Exception e) { 
			System.err.println("Error: " + e.getMessage());
		} 

	}

}
