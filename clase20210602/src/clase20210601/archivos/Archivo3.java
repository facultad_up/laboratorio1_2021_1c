package clase20210601.archivos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Archivo3 {

	public static void main(String[] args) {

		BufferedReader br = null;
		
		try {
			br = new BufferedReader(new FileReader(new File("file1.txt")));
			
			String linea;
			while ((linea = br.readLine()) != null) {
				System.out.println(linea);
			}
			
		} catch (IOException e) {

			e.printStackTrace();
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

	}

}
