package clase20210601.archivos;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class Archivo2 {

	public static void main(String[] args) {
		
		BufferedWriter bw = null;
		
		try {
			bw = new BufferedWriter(new FileWriter(new File("file1.txt")));
			bw.write("Primer archivo java\n");
			bw.write("1sarasa sarasa\n");
			bw.write("2sarasa sarasa\n");
			bw.write("3sarasa sarasa\n");
			
			int a=2, b= 0;
			System.out.println(a/b);
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			
			System.out.println(e.getMessage());
			throw new RuntimeException();
			
		} finally {
			if (bw != null)
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		
		System.out.println("Fin.");

	}

}
