package clase20210407.strategy;

public class Test {

	public static void main(String[] args) {
		Persona p = new Persona();
		p.saludar();
		
		p.cambiarEstadoDeAnimo(new Triste());
		p.saludar();
	}

}
