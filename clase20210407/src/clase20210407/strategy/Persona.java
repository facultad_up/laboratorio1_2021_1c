package clase20210407.strategy;

public class Persona {
	
	private EstadoDeAnimo estadoDeAnimo = new Feliz();
	
	public void cambiarEstadoDeAnimo(EstadoDeAnimo estadoDeAnimo) {
		this.estadoDeAnimo = estadoDeAnimo;
	}
	
	public void saludar() {
		// quiero que salude dependiendo del estado de animo
		this.estadoDeAnimo.saludar();
	}
}
