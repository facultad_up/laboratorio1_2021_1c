package clase20210407.inter;

public interface Caminador {
	public void caminar();
	public void correr();
	public void saltar();
}
