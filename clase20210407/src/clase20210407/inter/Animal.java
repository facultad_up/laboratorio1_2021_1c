package clase20210407.inter;

public abstract class Animal {
	protected float peso;
	protected String nombre;
	protected float altura;
	protected int edad;
	
	public Animal(float peso, String nombre, float altura, int edad) {
		this.peso = peso;
		this.nombre = nombre;
		this.altura = altura;
		this.edad = edad;
	}
		
	public abstract void comer();
	
}
