package clase20210407.inter;

public class Test {

	public static void main(String[] args) {
		Gato g = new Gato(200, "Pipi", 15, 1);
		g.correr();
		
		Caminador c = new Gato(200, "Pipi", 15, 1);
		c.caminar();
		
		Cuidador cuida = new Cuidador();
		cuida.pasear(g);
		
	}

}
