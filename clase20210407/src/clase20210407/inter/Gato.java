package clase20210407.inter;

public class Gato extends Animal implements Caminador, Nadador {
	private int bolasDePelosComidas;
	
	// Constructor
	public Gato(float peso, String nombre, float altura, int edad) {
		super(peso, nombre, altura, edad);

	}

	// Implementacion metodos heredados
	public void comer() {
		this.peso *= 1.01;
	}
	
	// Metodos propios de la clase Gato
	public void comerBolaPelo() {
		this.bolasDePelosComidas++;
	}

	public void maullar( ) {
		System.out.println("Miaauauauauaua");
	}

	// Metodos por las interefaces implementadas
	public void nadar() {
		System.out.println("Gato nadando");
	}

	public void flotar() {
		System.out.println("Gato flotando");
	}

	public void caminar() {
		System.out.println("Gato caminando");
	}

	public void correr() {
		System.out.println("Gato corriendo");
	}

	public void saltar() {
		System.out.println("Gato saltando");
	}
}
