package clase20210407.enumerados;

public class Test {

	public static void main(String[] args) {
		Colores c = Colores.AMARILLO;
		
		System.out.println(c);

		Auto a = new Auto(Colores.AMARILLO);
		System.out.println(a);
		
		if (c == Colores.AMARILLO) {
			System.out.println("Si es amarillo");
		}
		
		switch (c) {
		case AMARILLO:
			break;
		case AZUL:
			break;

		default:
			break;
		}
		
		System.out.println(Colores.AZUL.getRGB());
		
	}

}
