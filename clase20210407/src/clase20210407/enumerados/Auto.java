package clase20210407.enumerados;

public class Auto {
	private Colores color;
	
	public Auto(Colores color) {
		this.color = color;
	}
	
	public String toString() {
		return "Soy un auto de color " + this.color;
	}
}
