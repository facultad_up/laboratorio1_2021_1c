package clase20210609;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTable;

public class Tabla1 extends JFrame {
	
	
	public Tabla1() {
		JComboBox<EstadoCivil> estadoCivil = new JComboBox<EstadoCivil>();
		
		estadoCivil.addItem(new EstadoCivil("casado", "C", 1));
		estadoCivil.addItem(new EstadoCivil("soltero", "S", 2));
		estadoCivil.addItem(new EstadoCivil("viudo", "V", 3));
		estadoCivil.addItem(new EstadoCivil("divorciado", "D", 4));

		this.add(estadoCivil, BorderLayout.NORTH);

		estadoCivil.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				System.out.println(((EstadoCivil) estadoCivil.getSelectedItem()).getId());
				
			}
		});
		
		this.add(estadoCivil, BorderLayout.NORTH);
		
		
		String[] col = { "Uno", "Dos", "Tres" };
		String[][] filas = { { "0", "0", "0" }, { "0", "0", "0" } };

		JTable tabla = new JTable(filas, col);
		
		this.add(tabla);
	}
	
	public static void main(String[] args) {
		Tabla1 p1 = new Tabla1();
		p1.setVisible(true);
		p1.setDefaultCloseOperation(EXIT_ON_CLOSE);
		p1.setSize(400, 300);
		p1.setTitle("JList con cosas");
	}

}
