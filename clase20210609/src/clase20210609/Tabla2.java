package clase20210609;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public class Tabla2 extends JFrame {
	
	private final static String COLUMNA_ESTADO_CIVIL = "Estado Civil";
	
	
	public Tabla2() {
		JComboBox<EstadoCivil> estadoCivil = new JComboBox<EstadoCivil>();

		estadoCivil.addItem(new EstadoCivil("casado", "C", 1));
		estadoCivil.addItem(new EstadoCivil("soltero", "S", 2));
		estadoCivil.addItem(new EstadoCivil("viudo", "V", 3));
		estadoCivil.addItem(new EstadoCivil("divorciado", "D", 4));
		
		
		String[] col = { "Nombre", "Apellido", COLUMNA_ESTADO_CIVIL };
		
		DefaultTableModel modelo = new DefaultTableModel();
		modelo.setColumnIdentifiers(col);
		
		// Cargamos datos
		modelo.addRow(new Object[] {"N1", "A1", "casado"} );
		modelo.addRow(new Object[] {"N2", "A2", "soltero"} );
		modelo.addRow(new Object[] {"N3", "A3", "soltero"} );
		modelo.addRow(new Object[] {"N3", "A3", "soltero"} );
		modelo.addRow(new Object[] {"N3", "A3", "soltero"} );
		modelo.addRow(new Object[] {"N3", "A3", "soltero"} );
		modelo.addRow(new Object[] {"N3", "A3", "soltero"} );
		
		JTable tabla = new JTable(modelo);
		
		TableColumn tc = tabla.getColumn(COLUMNA_ESTADO_CIVIL);
		tc.setCellEditor(new DefaultCellEditor(estadoCivil));
		
		
		JButton b = new JButton("Ver");
		b.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				for(Object row : modelo.getDataVector()) {
					System.out.println(row);
				}
			}
		});
		
		this.add(b, BorderLayout.SOUTH);
		this.add(new JScrollPane(tabla));
	}

	public static void main(String[] args) {
		Tabla2 p1 = new Tabla2();
		p1.setVisible(true);
		p1.setDefaultCloseOperation(EXIT_ON_CLOSE);
		p1.setSize(400, 300);
		p1.setTitle("JTable con combo");
	}
}
