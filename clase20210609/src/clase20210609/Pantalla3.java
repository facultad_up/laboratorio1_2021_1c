package clase20210609;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

/*
 * FlowLayout
 * 
 * Comp1 Comp2 Comp3 
 * Comp4 .....
 */

public class Pantalla3  extends JFrame {
	private JButton boton1, boton2, boton3, boton4, boton5;
	
	public Pantalla3() {
		boton1 = new JButton("UNO");
		boton2 = new JButton("DOS");
		boton3 = new JButton("TRES");
		boton4 = new JButton("CUATRO");
		boton5 = new JButton("CINCO FIVE");
		
		this.setLayout(new FlowLayout(FlowLayout.RIGHT));

		this.add(boton1);
		this.add(boton2);
		this.add(boton3);
		this.add(boton4);
		this.add(boton5);
	}

	public static void main(String[] args) {
		Pantalla3 p = new Pantalla3();
		p.setVisible(true);
		p.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		p.setSize(400, 400);
		p.setTitle("Pantalla 3");
	}
}
