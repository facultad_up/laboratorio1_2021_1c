package clase20210609;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

class PanelBotones extends JPanel {
	
	public PanelBotones() {
		this.setLayout(new GridLayout(5, 1));
		
		JButton b1 = new JButton("Create");
		JButton b2 = new JButton("Read");
		JButton b3 = new JButton("Update");
		JButton b4 = new JButton("Delete las cosas");
		JButton b5 = new JButton("List");
		
		this.add(b1);
		this.add(b2);
		this.add(b3);
		this.add(b4);
		this.add(b5);
	}
}

class PanelTextoIlustrativo extends JPanel {
	
	public PanelTextoIlustrativo() {
		JTextArea text = new JTextArea();
		text.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit,\n" +
					 "sed do eiusmod tempor incididunt ut labore et dolore magna \n" +
					 "aliqua. Ut enim ad minim veniam, quis nostrud exercitation \n" +
					 "ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis\n" +
					 "aute irure dolor in reprehenderit in voluptate velit esse cillum\n" +
					 "dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidata\n" +
					 "non proident, sunt in culpa qui officia deserunt mollit anim id \n"+
					 "est laborum");
		
		this.add(text);
	}
}

class PanelIngreso extends JPanel {
	public PanelIngreso() {
		this.setLayout(new GridLayout(3, 1));
		
		JTextField t1 = new JTextField(100);
		JTextField t2 = new JTextField(100);
		JTextField t3 = new JTextField(100);
		
		this.add(t1);
		this.add(t2);
		this.add(t3);
	}
}

public class PantallaCompleta extends JFrame {
	
	public PantallaCompleta() {
		
		PanelBotones pb = new PanelBotones();					// Panel ROJO
		this.add(pb, BorderLayout.WEST);
		
		JPanel panel = new JPanel();							// Panel Rosa
		panel.setLayout(new GridLayout(2, 1));
		
		PanelTextoIlustrativo ti = new PanelTextoIlustrativo();	// Panel Marron
		panel.add(ti);
		
		PanelIngreso pi = new PanelIngreso();					// Panel Violeta
		panel.add(pi);
		
		this.add(panel);
	}
	
	public static void main(String[] args) {
		PantallaCompleta p1 = new PantallaCompleta();
		p1.setVisible(true);
		p1.setDefaultCloseOperation(EXIT_ON_CLOSE);
		p1.setSize(400, 300);
		p1.setTitle("Completa");
		
	}
}
