package clase20210609;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

/*
 * Sin Layout
 */
public class Pantalla5 extends JFrame {

	public Pantalla5() {
		this.setLayout(null);
		
		for(int i = 0; i < 20; i++) {
			JButton b = new JButton("Boton " + i);
			this.add(b);
			b.setBounds(50, 50+20*i, 100, 20);
		}
	}
	
	public static void main(String[] args) {
		Pantalla5 p = new Pantalla5();
		p.setVisible(true);
		p.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		p.setSize(400, 400);
		p.setTitle("Pantalla 4");

	}

}
