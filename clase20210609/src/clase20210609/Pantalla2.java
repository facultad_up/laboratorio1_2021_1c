package clase20210609;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;


/*
 * Layout
 *    LayoutManager:
 *    	- BorderLayout:     Default de los JFrame
 * 		- FlowLayout:		Default de los JPanel
 * 		- GridLayout
 * 		- null:			Sin layout	
 * 
 * 
 * 		- GridBagLayout
 * 
 * 
 *  BorderLayout
 *  		    	NORTH
 *  ------------------------------------------
 *          |						|
 *  WEST	|      *CENTER*         | EAST 
 *          |                       |
 *  ------------------------------------------
 *  				SOUTH
 *  
 */


public class Pantalla2 extends JFrame {
	private JButton boton1, boton2, boton3, boton4, boton5;
	
	
	public Pantalla2() {
		boton1 = new JButton("UNO");
		boton2 = new JButton("DOS");
		boton3 = new JButton("TRES");
		boton4 = new JButton("CUATRO");
		boton5 = new JButton("CINCO FIVE");
		
		this.add(boton1);
		this.add(boton2, BorderLayout.SOUTH);
		this.add(boton3, BorderLayout.NORTH);
		this.add(boton4, BorderLayout.WEST);
		this.add(boton5, BorderLayout.EAST);
	}
	
	public static void main(String[] args) {
		Pantalla2 p = new Pantalla2();
		p.setVisible(true);
		p.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		p.setSize(400, 400);
		p.setTitle("Pantalla 2");

	}

}
