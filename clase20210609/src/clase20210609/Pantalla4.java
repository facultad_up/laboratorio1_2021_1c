package clase20210609;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

/*
 * GridLayout
 */
public class Pantalla4 extends JFrame {

	public Pantalla4() {
		this.setLayout(new GridLayout(5, 4));
		
		for(int i = 0; i < 20; i++)
			this.add(new JButton("Boton " + i));
		
	}
	
	public static void main(String[] args) {
		Pantalla4 p = new Pantalla4();
		p.setVisible(true);
		p.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		p.setSize(400, 400);
		p.setTitle("Pantalla 4");

	}

}
