package clase20210609;

import java.awt.GridLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;

class MiSuperComponente extends JComponent {
	
	public MiSuperComponente(String nombre) {
		
		JPanel panel = new JPanel();
		
		JButton boton = new JButton("Click me!!");
		JTextField texto = new JTextField("Solo soy texto, " + nombre);
		
		panel .setLayout(new GridLayout(1, 2));
		panel .add(boton);
		panel .add(texto);
		
		this.add(panel);
	}
}

public class Lista2 extends JFrame {
	
	public Lista2() {
//		DefaultListModel<JComponent> modelo = new DefaultListModel<>();
//		
//		modelo.addElement(new MiSuperComponente("Uno"));
//		modelo.addElement(new MiSuperComponente("Dos"));
//		modelo.addElement(new MiSuperComponente("Tres"));
//		modelo.addElement(new MiSuperComponente("Cuatro"));
//		
//		JList<JComponent> lista = new JList<JComponent>();
//		lista.setModel(modelo);
//		
//		this.add(lista);
		
		DefaultListModel<JButton> modelo = new DefaultListModel<>();
		
		modelo.addElement(new JButton("Uno"));
		modelo.addElement(new JButton("Dos"));
		modelo.addElement(new JButton("Tres"));
		modelo.addElement(new JButton("Cuatro"));
		
		JList<JButton> lista = new JList<JButton>();
		lista.setModel(modelo);
		
		this.add(lista);
		
	}
	
	public static void main(String[] args) {
		Lista2 p1 = new Lista2();
		p1.setVisible(true);
		p1.setDefaultCloseOperation(EXIT_ON_CLOSE);
		p1.setSize(400, 300);
		p1.setTitle("JList con cosas");
	}

}
