package clase20210609;

class EstadoCivil {
	private String descripcion, descripcionCorta;
	private int id;

	public EstadoCivil() {
	}

	public EstadoCivil(String descripcion, String descripcionCorta, int id) {
		this.descripcion = descripcion;
		this.descripcionCorta = descripcionCorta;
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcionCorta() {
		return descripcionCorta;
	}

	public void setDescripcionCorta(String descripcionCorta) {
		this.descripcionCorta = descripcionCorta;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String toString() {
		return this.descripcion;
	}
}