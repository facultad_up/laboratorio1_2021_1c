package clase20210609;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JTextField;

public class Lista1 extends JFrame {
	private JList<String> lista = new JList<String>();
	private JTextField texto = new JTextField();
	private DefaultListModel<String> datos = new DefaultListModel<String>();

//	class TextoActionListener implements ActionListener {
//		public void actionPerformed(ActionEvent arg0) {
//			agregarTextoHandler(arg0);
//		}
//	}

	public Lista1() {
		datos.addElement("Uno");
		datos.addElement("Dos");
		datos.addElement("Tres");
		datos.addElement("Cuatro");

		lista.setModel(datos);

		// texto.addActionListener(new TextoActionListener());

		texto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agregarTextoHandler();
			}
		});

		lista.addMouseListener(new MouseListener() {

			public void mouseClicked(MouseEvent arg0) {
				clickedListHandler(arg0);
			}

			public void mouseEntered(MouseEvent arg0) {
//				System.out.println("mouseEntered");
			}

			public void mouseExited(MouseEvent arg0) {
//				System.out.println("mouseExited");
			}

			public void mousePressed(MouseEvent arg0) {
//				System.out.println("mousePressed");
			}

			public void mouseReleased(MouseEvent arg0) {
//				System.out.println("mouseReleased");
			}

		});

		this.add(texto, BorderLayout.NORTH);
		this.add(lista);
	}

	private void clickedListHandler(MouseEvent arg0) {
//		System.out.print(arg0.getButton());
//		System.out.print("      ");
//		System.out.println(arg0.getModifiers());
//		
		String seleccionado = lista.getSelectedValue();
		datos.removeElement(seleccionado);
	}

	private void agregarTextoHandler() {
		if (!texto.getText().isEmpty()) {
			datos.addElement(texto.getText());
			texto.setText("");
		}
	}

	public static void main(String[] args) {
		Lista1 p1 = new Lista1();
		p1.setVisible(true);
		p1.setDefaultCloseOperation(EXIT_ON_CLOSE);
		p1.setSize(400, 300);
		p1.setTitle("JList con texto");
	}

}
