package clase20210609;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Pantalla1 {
	
	public static void main(String[] args) {
		String leyenda = "Hola";
		
		JFrame frame = new  JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setSize(500, 400);
		frame.setVisible(true);
		
		JButton btn = new JButton("Click");
		frame.add(btn);
		
		btn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				System.out.println("Se clickeo boton: " + leyenda);
			}
			
		});
		
	}
}
