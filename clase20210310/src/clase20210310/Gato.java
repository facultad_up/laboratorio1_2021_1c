package clase20210310;

public class Gato {
	// Propiedades o variables de estado
	private String nombre;
	private float peso;
	private int edad;
	
	
	// Getters & Setters
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public float getPeso() {
		return peso;
	}
	
	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	// Metodo constructor por default
	public Gato(String nombre) {
		System.out.println("Me estan construyendo..." + nombre);
		this.nombre = nombre;
		this.peso = 500;
	}
	
	public Gato(String nombre, float peso) {
		this.nombre = nombre;
		this.peso = peso;
	}
	
	public Gato(float peso, String om) {
		
	}
	
	// Metodos
	public void alimentar(float cantComida) {
		peso = peso + cantComida * 0.1f;	// Sube de peso el 10% de lo que come
	}
	
	public void jugar(float tiempoMinutos) {
		peso = peso - tiempoMinutos * 0.2f;		
	}

	public String toString() {
		return "Gato [nombre=" + nombre + ", peso=" + peso + ", edad=" + edad + "]";
	}

	// Cambiar el comportamiento de un metodo heredado: Sobreescritura o override
//	public String toString() {
//		String s = "Soy ";
//		s += this.nombre; 	// s = s + this.nombre;
//		
//		if (this.peso < 1000) {
//			s += ", estoy bajo de peso.";
//		} else {
//			s += ", estoy pesado.";
//		}
//		
//		return s;
//	}
	
}
