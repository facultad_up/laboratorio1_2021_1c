package clase20210310;

public class Inicio {
	
	public static void main(String[] args) {
		Gato a = new Gato("Pepe");
		
		// a.nombre = "Pepe";

		System.out.println("Se creo un gato");
		System.out.println(a.getNombre());
		
		a.setNombre("Pipa");
		System.out.println(a.getNombre());
		
		Gato b = new Gato("Tom");
		System.out.println(b.getNombre());
		
		a.alimentar(200);
		System.out.println(a.getPeso());
		
		Gato c = new Gato("Pirulo", 230);
		
		System.out.println(a);
		System.out.println(b);
		System.out.println(c.toString());
	}
}
