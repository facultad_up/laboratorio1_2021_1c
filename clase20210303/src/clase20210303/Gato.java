package clase20210303;

public class Gato {
	// Aca van las propiedades
	String nombre;
	
	float peso;
	double largo;
	
	int edad;
	short cantidadPatas;
	long cantidadPelos;
	byte cantidadOjos;
	char cantidadUnias;
	
	
	// Aca van los metodos
	public void maullar() {
		System.out.println("Miaaaauuuuu " + nombre);
	}
	
	public static void main(String[] args) {
		// print('Hola Mundo')
		
		System.out.println("Hola Mundo");
		
		// syso y Control+Espacio
		
		Gato minino, gardfield;	// Variable de referencia a objeto del tipo Gato
		
		minino = new Gato();
		gardfield = new Gato();
		
		System.out.println(minino.edad);
		System.out.println(minino.nombre);
		minino.nombre = "Soy Minino";
		
		System.out.println(minino.nombre);
		System.out.println(gardfield.nombre);
		
		minino.maullar();
		gardfield.maullar();
		/*
		  Al crear objetos:
			-	Reservar la memoria.
			-	Construir el objeto.
			-	Necesito la referencia al objeto (memoria).

		*/
		
	}
}
